/**
 * lxroa
 * com.tz.oa.sysmanage.service.impl
 * UserService.java
 * 创建人:fangwanpeng 
 * 时间：2018年4月22日-下午1:23:43 
 * 2018潭州教育公司-版权所有
 */
package com.tz.oa.sysmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tz.oa.sysmanage.dao.UserMapper;
import com.tz.oa.sysmanage.entity.User;
import com.tz.oa.sysmanage.service.IUserService;
import com.tz.oa.sysmanage.vo.UserExample;
import com.tz.oa.sysmanage.vo.UserExample.Criteria;

/**
 * 
 * UserService 创建人:fangwanpeng 时间：2018年4月22日-下午1:23:43
 * 
 * @version 1.0.0
 * 
 */

@Service
public class UserService implements IUserService {

	@Resource
	private UserMapper userMapper;

	UserExample userExample = null;

	public boolean loginUser(User user) {
		if (user != null) {
			String loginName = user.getLoginName();
			String password = user.getPassword();
			userExample = new UserExample();
			UserExample.Criteria criteria = userExample.or();
			if (!loginName.isEmpty()) {
				criteria.andLoginNameEqualTo(loginName);
			}
			List<User> userList = userMapper.selectByExample(userExample);
			if(password.equals(userList.get(0).getPassword())){
				return true;
			}else{
				return false;
			}
			
		} else {
			return false;
		}

		
	}

}
