/**
 * lxroa
 * com.tz.oa.sysmanage.controller
 * LoginContoller.java
 * 创建人:fangwanpeng 
 * 时间：2018年4月21日-下午10:50:03 
 * 2018潭州教育公司-版权所有
 */
package com.tz.oa.sysmanage.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tz.oa.framework.dto.Opt;
import com.tz.oa.sysmanage.entity.User;
import com.tz.oa.sysmanage.service.impl.UserService;

/**
 * 
 * LoginContoller
 * 创建人:fangwanpeng
 * 时间：2018年4月21日-下午10:50:03 
 * @version 1.0.0
 * 
 */
@Controller
public class LoginController {
	
	private static Logger logger = Logger.getLogger(LoginController.class);
	@Resource
	private UserService userService;
	
	@RequestMapping("/gotoLogin")
	public String gotoLogin(){
		return "login";
	}
	
	@RequestMapping("/login")
	@ResponseBody
	public Opt login(User user,Model model,HttpSession session){
		Opt opt =new Opt();
		if(user!=null){
			Boolean flag =userService.loginUser(user);
			if(flag){
				opt.setMessage("登陆成功");
				session.setAttribute("user", user);
				
			}else{
				opt.setMessage("登陆失败");
				opt.setSuccess(false);
			}
		}else{
			opt.setMessage("登陆失败");
			opt.setSuccess(false);
		}
		
		logger.info("登陆用户名:"+user.getLoginName());
		return opt;
	}
	
	@RequestMapping("/index")
	public String main(){
		
		return "/main/main";
	}
}
