/**
 * lxroa
 * com.tz.oa.sysmanage.service
 * IUserService.java
 * 创建人:fangwanpeng 
 * 时间：2018年4月22日-下午1:24:09 
 * 2018潭州教育公司-版权所有
 */
package com.tz.oa.sysmanage.service;

import java.util.List;

import com.tz.oa.sysmanage.entity.User;

/**
 *user 业务类
 * IUserService
 * 创建人:fangwanpeng
 * 时间：2018年4月22日-下午1:24:09 
 * @version 1.0.0
 * 
 */
public interface IUserService {
	/**
	 * 
	 * 登陆验证
	 * 方法名：loginUser
	 * @param loginName
	 * @param password
	 * @return User
	 * @exception
	 */
	public boolean loginUser(User user);
}
