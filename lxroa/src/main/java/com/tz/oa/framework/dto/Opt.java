/**
 * lxroa
 * com.tz.oa.framework.dto
 * opt.java
 * 创建人:fangwanpeng 
 * 时间：2018年4月22日-下午9:28:57 
 * 2018潭州教育公司-版权所有
 */
package com.tz.oa.framework.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * opt
 * 创建人:fangwanpeng
 * 时间：2018年4月22日-下午9:28:57 
 * @version 1.0.0
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Opt implements Serializable{
	
	private boolean success=true;
	private String message;
	private String statusCode;
	private String data;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
