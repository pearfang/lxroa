/**
 * lxroa
 * com.tz.oa.framework.interceptor
 * LoginInterceptor.java
 * 创建人:fangwanpeng 
 * 时间：2018年4月25日-上午12:08:25 
 * 2018潭州教育公司-版权所有
 */
package com.tz.oa.framework.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.tz.oa.sysmanage.entity.User;

/**
 * 用户登陆认证拦截器  
 * LoginInterceptor
 * 创建人:fangwanpeng
 * 时间：2018年4月25日-上午12:08:25 
 * @version 1.0.0
 * 
 */
public class LoginInterceptor implements HandlerInterceptor{
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		// TODO Auto-generated method stub
		//得到请求的地址
		String url =request.getRequestURI();
		if(url.indexOf("login")>=0||url.indexOf("gotoLogin")>=0){
			return true;
		}
		//如果不是公开地址,进行session判断,如果session有记录,则返回true
				//如果session里面没有值,则转发到登陆页面重新输入用户名和密码进行验证
				HttpSession session = request.getSession();
				User user = (User) session.getAttribute("user");
				if(user!=null){
					return true;
				}else{
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}
			return false;
	}
	
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler,Exception exception)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
}
